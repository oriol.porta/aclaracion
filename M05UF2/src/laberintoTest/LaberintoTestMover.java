package laberintoTest;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import laberinto.Laberinto;

@RunWith(Parameterized.class)

public class LaberintoTestMover {
	
	private int [][] mapa;
	private int [] pos;
	private int [] paso;
	private boolean resul;
	
	public LaberintoTestMover (int [][] mapa, int [] pos, int [] paso, boolean resul) {
		this.mapa = mapa;
		this.pos = pos;
		this.paso = paso;
		this.resul = resul;
	}

	@Parameters
	public static Collection <Object[]> numeros() {
		return Arrays.asList (new Object[][] {
			{new int[][] {{1,1,3},{1,0,1},{2,1,1}}, new int[] {1,1}, new int[] {-1, 0}, true},
			{new int[][] {{2,2,1},{3,0,2},{1,2,1}}, new int[] {1,1}, new int[] {0, 1}, true},
			{new int[][] {{3,2,1},{2,0,2},{2,1,1}}, new int[] {1,1}, new int[] {1, 0}, true},
			{new int[][] {{2,3,1},{3,0,3},{3,3,1}}, new int[] {1,1}, new int[] {-1, 0}, false},
			{new int[][] {{2,1,1},{1,0,3},{1,2,2}}, new int[] {1,1}, new int[] {0, 1}, false}
		});
		
	}

	@Test
	public void testMover() {
		boolean res = Laberinto.mover(mapa, pos, paso);
			assertEquals(res, resul);
		//fail("Not yet implemented");
	}

}
