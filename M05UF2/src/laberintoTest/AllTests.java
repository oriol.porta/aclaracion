package laberintoTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LaberintoTestComprobar.class, LaberintoTestMover.class })
public class AllTests {

}
